# Majframe bootstrap

![Packagist Version](https://img.shields.io/packagist/v/majframe/bootstrap?label=version)
![PHP from Packagist](https://img.shields.io/packagist/php-v/majframe/bootstrap)
![Packagist](https://img.shields.io/packagist/l/majframe/bootstrap)
![Libraries.io dependency status for GitHub repo](https://img.shields.io/librariesio/github/majframe/bootstrap)
![Packagist](https://img.shields.io/packagist/dt/majframe/bootstrap)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

* Majframe framework
* PHP 7.4

### Installing

Recommended installation is using composer.

```
composer require majframe/bootstrap
```

## Authors

* **Ondřej Maxa** - ondrej@maxa.expert - http://ondrej.maxa.expert/

## License

This project is licensed under the GPL v3.0 License - see the [LICENSE.md](LICENSE.md) file for details
