<?php
declare(strict_types=1);

namespace Majframe\Bootstrap;

use Oops\WebpackNetteAdapter\DI\WebpackExtension;
use Contributte\Console\DI\ConsoleExtension;
use Nettrine\Annotations\DI\AnnotationsExtension;
use Nettrine\Cache\DI\CacheExtension;
use Nettrine\Migrations\DI\MigrationsExtension;
use Nettrine\Fixtures\DI\FixturesExtension;
use Nettrine\DBAL\DI\DbalExtension;
use Nettrine\DBAL\DI\DbalConsoleExtension;
use Nettrine\ORM\DI\OrmExtension;
use Nettrine\ORM\DI\OrmCacheExtension;
use Nettrine\ORM\DI\OrmConsoleExtension;
use Nettrine\ORM\DI\OrmAnnotationsExtension;

class Configurator extends \Nette\Configurator
{
    /**
     * {@inheritDoc}
     */
	public function __construct()
	{
        parent::__construct();
        $this->addDefaultExtensions();
        $this->configs[] = realpath(__DIR__ . '/../default.neon');
    }
    
    protected function addDefaultExtensions(): void
    {
        $this->defaultExtensions['webpack'] = [WebpackExtension::class, ['%debugMode%', '%consoleMode%']];
        $this->defaultExtensions['console'] = [ConsoleExtension::class, ['%consoleMode%']];
        $this->defaultExtensions['nettrine.annotations'] = AnnotationsExtension::class;
        $this->defaultExtensions['nettrine.cache'] = CacheExtension::class;
        $this->defaultExtensions['nettrine.migrations'] = MigrationsExtension::class;
        $this->defaultExtensions['nettrine.fixtures'] = FixturesExtension::class;
        $this->defaultExtensions['nettrine.dbal'] = DbalExtension::class;
        $this->defaultExtensions['nettrine.dbal.console'] = [DbalConsoleExtension::class, ['%consoleMode%']];
        $this->defaultExtensions['nettrine.orm'] = OrmExtension::class;
        $this->defaultExtensions['nettrine.orm.cache'] = OrmCacheExtension::class;
        $this->defaultExtensions['nettrine.orm.console'] = [OrmConsoleExtension::class, ['%consoleMode%']];
        $this->defaultExtensions['nettrine.orm.annotations'] = OrmAnnotationsExtension::class;
    }

    /**
     * {@inheritDoc}
     */
	protected function getDefaultParameters(): array
	{
		$trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
		$last = end($trace);
		$debugMode = static::detectDebugMode();
        $loaderRc = class_exists(ClassLoader::class) ? new \ReflectionClass(ClassLoader::class) : null;
        if (isset($trace[2]['file'])) {
            $appDir = dirname($trace[2]['file']);
            $rootDir = realpath($appDir.'/..');
        }
		return [
			'appDir' =>  $appDir ?: null,
            'rootDir' => $rootDir ?: null,
			'wwwDir' => isset($last['file']) ? dirname($last['file']) : null,
			'vendorDir' => $loaderRc ? dirname($loaderRc->getFileName(), 2) : null,
			'debugMode' => $debugMode,
			'productionMode' => !$debugMode,
            'consoleMode' => PHP_SAPI === 'cli'
		];
	}
}